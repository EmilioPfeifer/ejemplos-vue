# hello-world

## Project setup, dentro del directorio
```
npm install
```

### Compiles y visualizacion
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
